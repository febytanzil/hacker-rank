package com.feby.codility.grab.task1;

/**
 * Created by Asus on 6/12/2016.
 */
public class Solution {
    public int solution(String E, String L) {
        // write your code in Java SE 8
        int cost = 2;

        String[] ent = E.split(":");
        String[] exi = L.split(":");

        int entHour = Integer.parseInt(ent[0]);
        int exiHour = Integer.parseInt(exi[0]);
        int entMin = Integer.parseInt(ent[1]);
        int exiMin = Integer.parseInt(exi[1]);

        int distanceMin = ((exiHour * 60) + exiMin) - ((entHour * 60) + entMin);
        int i = 0;

        while (0 < distanceMin) {
            distanceMin = distanceMin - 60;
            i++;
            if (1 == i) {
                cost += 3;
            } else {
                cost += 4;
            }
        }

        return cost;
    }
}
