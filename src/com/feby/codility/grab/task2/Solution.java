package com.feby.codility.grab.task2;

/**
 * Created by Asus on 6/12/2016.
 */
public class Solution {
    public int solution(int[] A) {
        // write your code in Java SE 8
        int cost = 0;
        int first = A[0];
        int dist = 0;
        int count = 0;

        for (int i = 0; i < A.length; i++) {
            dist = A[i] - first;
            if (dist > 7) {
                first = A[i];
                count = 0;
            }
            count++;
            System.out.println("So... the scheduled date now I read is " + A[i] + " and the first date is" + first + " and distance now-first is " + dist + " and count is " + count);

            if (6 <= dist && count > 3) {
                System.out.println("Found disc w 7 days, set first to " + A[i]);
                cost += 7;
                first = A[i];
                count = 0;
            }
        }

        if (count > 0) {
            cost += (count * 2);
        }
        if (25 < cost) {
            cost = 25;
        }

        return cost;
    }
}
