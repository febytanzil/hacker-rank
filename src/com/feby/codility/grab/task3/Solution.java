package com.feby.codility.grab.task3;

import java.util.LinkedList;

/**
 * Created by Asus on 6/12/2016.
 */
public class Solution {
    public int[] solution(int[] T) {
        // write your code in Java SE 8
        int v = T.length;
        int e = v - 1;
        int[] level = new int[v];
        int[][] arr = new int[v][v];
        LinkedList<Integer> q = new LinkedList<>();
        int source = -1;
        int depth = 0;

        for (int i = 0; i < v; i++) {
            if (i != T[i]) {
                arr[i][T[i]] = 1;
                arr[T[i]][i] = 1;
            } else {
                source = i;
            }
        }

        //System.out.println("Source is " + source);
        q.add(source);
        level[source] = 0;

        while (!q.isEmpty()) {
            int visiting = q.pop();

            if (level[visiting] > depth) {
                depth++;
            }

            //System.out.println("Now I'm visiting node " + visiting + " with distance from source " + level[visiting] + " in level " + depth);

            for (int j = 0; j < v; j++) {
                // check if nodes interconnecting
                if (1 == arr[visiting][j]) {
                    if (level[j] == 0 && j != source) {
                        level[j] = depth + 1;
                        q.add(j);
                        //System.out.println("Found adj to node " + j + " set distance to " + level[j]);
                    }
                }
            }

            //System.out.println("Layer depth is " + depth);
        }

        int[] r = new int[e];

        for (int i = 0; i < v; i++) {
            if (i != source) {
                int dist = level[i];
                r[dist - 1] += 1;
            }
        }

        return r;
    }
}
