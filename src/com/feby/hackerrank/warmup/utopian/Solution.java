package com.feby.hackerrank.warmup.utopian;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int T = in.nextInt();
        
        for (int i = 0; i < T; i++) {
            int c = in.nextInt();
            System.out.println(calculate(c));
        }
    }
    
    private static int calculate(int cycle) {
        int result = 1;
        
        for (int i = 0; i < cycle; i++) {
            if (0 == i % 2) {
                result = result * 2;
            } else {
                result++;
            }
        }
        
        return result;
    }
}
