package com.feby.hackerrank.warmup.maxxor;

import java.util.*;

public class Solution {
/*
 * Complete the function below.
 */

    static int maxXor(int l, int r) {
        int max = 0;
        
        for (int i = l; i <= r; i++) {
        	for (int j = l; j <= r; j++) {
        		int xor = xor(i, j);

        		if (max < xor) {
        			max = xor;
        		}
        	}
        }
        
        return max;
    }
    
    static int xor(int l, int r) {
    	String first = Integer.toBinaryString(l);
        String second = Integer.toBinaryString(r);
        String result = "";
        int limiter = first.length();
        int initLength = 0;
        
        if (first.length() > second.length()) {
        	limiter = first.length();
        	initLength = second.length();
        	for (int j = 0; j < first.length() - initLength; j++) {
        		second = "0" + second;
        	}
        } else if (first.length() < second.length()) {
        	limiter = second.length();
        	initLength = first.length();
        	for (int j = 0; j < second.length() - initLength; j++) {
        		first = "0" + first;
        	}
        }

        for (int i = 0; i < limiter; i++) {
        	if (first.charAt(i) == second.charAt(i)) {
        		result = result + "0";
        	} else {
        		result = result + "1";
        	}
        }
        
        return Integer.parseInt(result, 2);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int res;
        int _l;
        _l = Integer.parseInt(in.nextLine());
        
        int _r;
        _r = Integer.parseInt(in.nextLine());
        
        res = maxXor(_l, _r);
        System.out.println(res);
        
    }
}
