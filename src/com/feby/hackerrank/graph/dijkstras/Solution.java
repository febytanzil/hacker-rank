package com.feby.hackerrank.graph.dijkstras;

import java.io.PrintWriter;
import java.util.*;

/**
 * Created by Asus on 6/11/2016.
 */
public class Solution {
    private static int[][] arr;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter writer = new PrintWriter(System.out, false);
        int T = in.nextInt();

        for (int i = 0; i < T; i++) {
            PriorityQueue<Node>  q = new PriorityQueue<>();
            int v = in.nextInt();
            int e = in.nextInt();
            arr = new int[v][v];
            List<Node> res = new ArrayList<>(v);

            for (int j = 0; j < e; j++) {
                int v1 = in.nextInt() - 1;
                int v2 = in.nextInt() - 1;
                int w = in.nextInt();
                //System.out.println("setting edge for " + (v1 + 1) + "-" + (v2 + 1));
                // handling f-ing duplicate edges inputs
                if (arr[v1][v2] == 0) {
                    arr[v1][v2] = w;
                    arr[v2][v1] = w;
                } else if (arr[v1][v2] > 0 && w < arr[v1][v2]) {
                    arr[v1][v2] = w;
                    arr[v2][v1] = w;
                }
            }

            int source = in.nextInt() - 1;
            //System.out.println("The source is node " + (source + 1));
            Node root = new Node(source);
            root.setDistance(0);
            q.add(root);

            for (int j = 0; j < v; j++) {
                if (source != j) {
                    Node notFound = new Node(j);
                    q.add(notFound);
                    res.add(notFound);
                } else {
                    res.add(root);
                }
            }

            while (!q.isEmpty()) {
                Node visiting = q.poll();
                visiting.setVisited(true);
                //System.out.println("Now visiting node " + (visiting.getNodeName() + 1) + " removed from queue");
                for (int j = 0; j < v; j++) {
                    int w = arr[visiting.getNodeName()][j];
                    if (0 < w) {
                        //System.out.println("Found adj to node " + (j + 1));
                        Node adj = res.get(j);
                        if ((visiting.getDistance() + w) < adj.getDistance()) {
                            adj.setDistance((visiting.getDistance() + w));
                            //System.out.println("Set distance node " + (j + 1) + " to " + (visiting.getDistance() + w));
                            requeue(q, res);
                        }
                    }
                }

                //System.out.println("current queue contents are " + Arrays.toString(q.toArray()));
            }

            for (Node r : res) {
                if (!r.equals(root)) {
                    if (999 == r.getDistance()) {
                        writer.print("-1 ");
                    } else {
                        writer.print(r.getDistance() + " ");
                    }
                }
            }

            writer.println();
        }

        writer.flush();
    }

    private static void requeue(PriorityQueue<Node> q, List<Node> arr) {
        q.clear();
        //System.out.println("Cleared size = " + q.size());
        for (Node n : arr) {
            if (!n.isVisited()) {
                q.add(n);
            }
        }
    }

    private static class Node implements Comparable<Node> {
        private int distance = 999;
        private int nodeName;
        private boolean visited = false;

        Node(int nodeName) {
            this.nodeName = nodeName;
        }

        int getNodeName() {
            return nodeName;
        }

        void setNodeName(int nodeName) {
            this.nodeName = nodeName;
        }

        int getDistance() {
            return distance;
        }

        void setDistance(int distance) {
            this.distance = distance;
        }

        boolean isVisited() {
            return visited;
        }

        void setVisited(boolean visited) {
            this.visited = visited;
        }

        @Override
        public int compareTo(Node o) {
            if (null == o) {
                throw new NullPointerException();
            }

            return this.distance - o.getDistance();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (!(obj instanceof Node))
                return false;

            Node o = (Node) obj;

            return this.nodeName == o.getNodeName();
        }

        @Override
        public String toString() {
            return "[name = " + (this.nodeName + 1) + " distance = " + this.distance + "]";
        }
    }
}