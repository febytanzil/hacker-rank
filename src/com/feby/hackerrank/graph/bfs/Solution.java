package com.feby.hackerrank.graph.bfs;

import java.io.PrintWriter;
import java.util.*;

/**
 * Created by Asus on 6/11/2016.
 */
public class Solution {
    private static int[][] arr;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PrintWriter writer = new PrintWriter(System.out, false);
        int T = in.nextInt();

        for (int i = 0; i < T; i++) {
            // INIT START
            //System.out.println("Case " + (i + 1));
            int v = in.nextInt();
            int e = in.nextInt();
            //System.out.println("Vertices " + v + " edges " + e);
            arr = new int[v][v];
            LinkedList<Integer> q = new LinkedList<>();
            int[] dist = new int[v];
            int[] level = new int[v];

            for (int j = 0; j < v; j++) {
                dist[j] = -1;
            }

            for (int j = 0; j < e; j++) {
                int v1 = in.nextInt() - 1;
                int v2 = in.nextInt() - 1;
                //System.out.println("setting edge for " + (v1 + 1) + "-" + (v2 + 1));
                arr[v1][v2] = 1;
                arr[v2][v1] = 1;
            }

            //System.out.println("Source is " + (source.intValue() + 1));
            int depth = 0;
            Integer source = in.nextInt() - 1;
            dist[source] = 0;
            level[source] = 0;
            q.add(source);
            // INIT END

            while (!q.isEmpty()) {
                Integer visiting = q.pop();

                if ((level[visiting]) > depth) {
                    depth++;
                }

                //System.out.println("Now I'm visiting node " + (visiting.intValue() + 1) + " with distance from source " + dist[visiting.intValue()] + " in level " + depth);

                for (int k = 0; k < v; k++) {
                    // check if nodes interconnecting
                    if (1 == arr[visiting][k]) {
                        // check if already dist
                        if (dist[k] == -1) {
                            dist[k] = (depth + 1) * 6;
                            level[k] = depth + 1;
                            //System.out.println("Found adj to node " + (k + 1) + " set distance to " + dist[k] + " level to " + level[k]);
                            q.add(k);
                        } /*else {
                            System.out.println("Already visit node " + (k + 1));
                        }*/
                    }
                }

                //System.out.println("Layer depth is " + depth);
            }

            // print result
            for (int res : dist) {
                if (0 != res) {
                    writer.print(res + " ");
                }
            }
            writer.println();
        }

        writer.flush();
    }

}
