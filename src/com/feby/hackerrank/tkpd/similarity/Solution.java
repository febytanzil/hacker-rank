package com.feby.hackerrank.tkpd.similarity;

import java.io.PrintWriter;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		PrintWriter writer = new PrintWriter(System.out, false);
		
		int T = in.nextInt();
		
		for (int i = 0; i < T; i++) {
            String input = in.next();
            writer.println(calculate(input));
        }
		
		writer.flush();
	}
	
	private static int calculate(String input) {
		String sub = "";
		int counter = input.length();
		int index = 0;
		
		for (int i = 1; i < input.length(); i++) {
			sub = input.substring(i); 
			index = compare(input, sub);
			counter = counter + index;
		}
		
		return counter;
	}
	
	private static int compare(String one, String two) {
		int counter = 0;
		
		for (int i = 0; i < two.length(); i++) {
			if (one.startsWith(two.substring(0, two.length() - i))) {
				counter = two.substring(i).length();
				break;
			}
		}
		
		return counter;
	}

}
