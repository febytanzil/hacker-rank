package com.feby.hackerrank.tkpd.stack;

import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Stack;

public class Solution {
	
	private static Stack<Long> q = new Stack<Long>();

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		PrintWriter writer = new PrintWriter(System.out, false);
		
		int T = in.nextInt();
		
		for (int i = 0; i < T; i++) {
            String op = in.next();
            
            if ("push".equals(op)) {
            	long input = in.nextLong();
            	q.add(Long.valueOf(input));
            } else if (("pop").equals(op)) {
            	q.pop();
            } else if ("inc".equals(op)) {
            	long length = in.nextLong();
            	long inc = in.nextLong();
            	int qSize = q.size();
            	Long[] data = new Long[qSize];
            	
            	q.copyInto(data);
            	for (int j = 0; j < length; j++) {
            		data[j] = data[j] + inc;
            	}
            	q.clear();
            	for (int j = 0; j < qSize; j++) {
            		q.add(data[j]);
            	}
            }
            
            if (0 == q.size()) {
            	writer.println("EMPTY");
        	} else {
        		writer.println(q.peek());
        	}
        }
		
		writer.flush();
	}

}
