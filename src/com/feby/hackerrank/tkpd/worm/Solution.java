package com.feby.hackerrank.tkpd.worm;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int n = in.nextInt();
		
		if (0 < n) {
			int u = in.nextInt();
			int d = in.nextInt();
			
			System.out.println(calculate(n, u, d));
		}
	}
	
	private static int calculate(int n, int u, int d) {
		int minutes = 0;
		
		while (n > 0) {
			if (0 == minutes % 2) {
				n = n - u;
			} else {
				n = n + d;
			}
			
			minutes++;
		}
		
		return minutes;
	}

}
