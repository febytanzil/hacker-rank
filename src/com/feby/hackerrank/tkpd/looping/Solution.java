package com.feby.hackerrank.tkpd.looping;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int n = in.nextInt();
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (j - i < 0) {
					System.out.print(0 + " ");
				} else {
					if (n == j + 1) {
						System.out.print(j + 1);
					} else {
						System.out.print(j + 1 + " ");
					}
				}
			}
			System.out.println();
		}
	}

}
