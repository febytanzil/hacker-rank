package com.feby.hackerrank.strings.twostrings;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		PrintWriter writer = new PrintWriter(System.out, false);
		
		for (int i = 0; i < T; i++) {
			if (isCommon(in.next(), in.next())) {
				writer.println("YES");
			} else {
				writer.println("NO");
			}
		}
		
		writer.flush();
	}
	
	private static boolean isCommon(String first, String second) {
		Set<Character> sets = new HashSet<Character>();
		
		for (int i = 0; i < first.length(); i++) {
			sets.add(first.charAt(i));
		}
		
		for (Character c : sets) {
			if (-1 != second.indexOf(c)) {
				return true;
			}
		}
		
		return false;
	}
}
