package com.feby.hackerrank.strings.biggreat;

import java.io.PrintWriter;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		PrintWriter writer = new PrintWriter(System.out, false);
		
		for (int i = 0; i < T; i++) {
			String input = in.next();
			String result = doIncrease(input);
			
			if (input.equals(result)) {
				writer.println("no answer");
			} else {
				writer.println(result);
			}
		}
		
		writer.flush();
	}
	
	private static String doIncrease(String init) {
		String temp = init;
		String result = "z" + init;
		String swap = "";
		
		for (int i = temp.length() - 2; i >= 0; i--) {
			for (int j = temp.length() - 1; j >= i; j--) {
				swap = doSwap(temp, i, j);
				if (result.compareTo(swap) > 0 && init.compareTo(swap) < 0) {
					result = swap;
					i = temp.length() - 1;
					temp = result;
					break;
				}
			}
		}
		
		if (init.equals(result.substring(1))) {
			return init;
		}
		
		return result;
	}
	
	private static String doSwap(String string, int thisIndex, int withIndex) {
		StringBuilder sb = new StringBuilder(string);
		sb.setCharAt(thisIndex, string.charAt(withIndex));
		sb.setCharAt(withIndex, string.charAt(thisIndex));
		
		return sb.toString();
	}

}
